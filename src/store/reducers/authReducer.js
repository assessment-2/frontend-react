import { LOGIN, LOGOUT, REGISTER } from '../types';

const initialState = [];


const authReducer = (state = initialState, action) => {
    switch(action.type){
        case LOGIN:
            return [...state, {token: action.payload}];
        case LOGOUT:
            return [...state, {token: null}];
        case REGISTER:
            return [...state, {token: action.payload}];
        default:
            return state;
    }
}

export default authReducer;