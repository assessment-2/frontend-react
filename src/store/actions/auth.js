import React from "react";
import { 
    LOGIN,
    REGISTER,
    LOGOUT,
 } from "../types";
 import { Api } from "../../Api";


export const Login = async (email, password) => {
    const res = await Api.post('/auth/login', { email, password});
    const action = {type: LOGIN, payload: res.data};
    return action;
}

export const RegisterAction = async (data) => {
    const res = await Api.post('/auth/register', data );
    const action = {type: REGISTER, payload: res.data};
    return action;
}