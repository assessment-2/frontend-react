import React from "react";
import { BrowserRouter, Routes, Route, Link } from "react-router-dom";
import Home from './pages/Home';
import Login from './pages/Login';
import Register from './pages/Register';
import UploadFile from './pages/UploadFile';




function App() {
  return (
    <BrowserRouter>
      <Routes >
        <Route path="/" element={<Home/>} />
        <Route path="/login" element={<Login/>} />
        <Route path="/register" element={<Register/>} />
        <Route path="/upload" element={<UploadFile/>} />
      </Routes>
    </BrowserRouter>
  );
}

export default App;
