import React from "react";
import { useState } from "react";
import { useDispatch } from "react-redux";
import { RegisterAction } from "../../store/actions/auth";
const Register = () => {
    const [data, setData] = useState({name: '', email:'', password:'', confirm_password:'', age: ''});
    const [picture, setPicture] = useState(null);
    const dispatch = useDispatch();
    const onChange = (e) => {
        setData({...data, [e.target.value]: e.target.value} );
    }

    const onSubmit = (e) => {
        e.preventDefault();
        const { name, email, password, confirm_password, age} = data;
        const form = new FormData();
        form.append('name', name);
        form.append('email', email);
        form.append('password', password);
        form.append('confirm_password', confirm_password);
        form.append('age', age);
        form.append('picture', picture);

        dispatch(RegisterAction(form));
    }

    return (
        <div className="container">
        <div className="row">
            <h3>Register</h3>
        </div>
        <div className="row">
        <form onSubmit={onSubmit}>
            <div className="form-group">
                <label htmlFor="email">Name:</label>
                <input
                type="text"
                className="form-control"
                placeholder="Enter Name"
                id="name"
                name="name"
                value={data.name}
                onChange={onChange}
                />
            </div>
            <div className="form-group">
                <label htmlFor="email">Email address:</label>
                <input
                type="email"
                className="form-control"
                placeholder="Enter email"
                id="email"
                name="email"
                value={data.email}
                onChange={onChange}
                />
            </div>
            <div className="form-group">
                <label htmlFor="pwd">Password:</label>
                <input
                type="password"
                className="form-control"
                placeholder="Enter password"
                id="pwd"
                name="password"
                value={data.password}
                onChange={onchange}


                />
            </div>
            <div className="form-group">
                <label htmlFor="pwd">Confirm Password:</label>
                <input
                type="password"
                className="form-control"
                placeholder="Enter confirm password"
                id="pwd"
                name="confirm_password"
                value={data.confirm_password}
                onChange={onChange}


                />
            </div>
            <div className="form-group">
                <label htmlFor="email">Age:</label>
                <input
                type="number"
                className="form-control"
                placeholder="Enter age"
                id="age"
                name="age"
                value={data.age}
                onChange={onChange}

                />
            </div>
            <div className="form-group">
                <label htmlFor="picture">Picture:</label>
                <input
                type="file"
                className="form-control"
                placeholder="Enter email"
                id="picture"
                name="picture"
                value={picture}
                onChange={(e)=> setPicture(e.target.files[0])}

                />
            </div>
            <button type="submit" className="btn btn-primary">
                Submit
            </button>
        </form>
        </div>
    </div>  
    );
}

export default Register;