import React from "react";
import { useState } from "react";
const Login = () => {
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');

    const onSubmit = (e) => {
        e.preventDefault();
        

    }
    return (
        <div className="container">
        <div className="row">
            <h3>Login</h3>
        </div>
        <div className="row">
        <div className="mx-auto">
        <form onSubmit={onSubmit}>
            <div className="form-group">
                <label htmlFor="email">Email address:</label>
                <input
                type="email"
                className="form-control"
                placeholder="Enter email"
                id="email"
                name="email"
                value={email}
                onChange={(e) => setEmail(e.target.value)}
                />
            </div>
            <div className="form-group">
                <label htmlFor="pwd">Password:</label>
                <input
                type="password"
                className="form-control"
                placeholder="Enter password"
                id="pwd"
                name="password"
                value={password}
                onChange={(e) => setPassword(e.target.value)}
                />
            </div>
            <button type="submit" className="btn btn-primary">
                Submit
            </button>
        </form>
        </div>
       
        </div>
    </div>  
    );
}

export default Login;